#require 'bigdecimal'
#require 'bigdecimal/util'

def ftoc(temperature_fahrenheit)
  ((temperature_fahrenheit-32)/1.8).round
end

def ctof(temperature_celsius)
  answer = temperature_celsius*1.8+32
end
