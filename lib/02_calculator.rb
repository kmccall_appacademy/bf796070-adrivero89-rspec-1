def add(number1,number2)
  number1+number2
end

def subtract(number1,number2)
  number1-number2
end

def sum(array)
  return 0 if array.empty?
  array.reduce(:+)
end

def multiply(number1,number2)
  number1*number2
end

def power(number1,number2)
  number1**number2
end

def factorial(number)
  return 1 if number == 0
  (1..number).reduce(:*)
end
