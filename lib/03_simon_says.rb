def echo(phrase)
    phrase
end

def shout(phrase) 
  phrase.upcase
end

def repeat(phrase, times=2)
  ([phrase]*times).join(" ")
end

def start_of_word(word,number_letters)
  word[0..number_letters-1]
end

def first_word(phrase)
  phrase.split[0]
end

def titleize(phrase)
  little_words = ["the", "over", "and", "a"]
  split_phrase = phrase.split
  capitalized = split_phrase.map.with_index do |word,idx|
    if idx==0 || little_words.include?(word) == false
      word.capitalize
    else
      word
    end
  end

  capitalized.join(" ")
end
