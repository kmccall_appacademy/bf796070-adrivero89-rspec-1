def translate(phrase)
  split_phrase = phrase.split

  pig_latin = split_phrase.map do |word|
    vowel_idx = first_vowel(word)
    if vowel_idx == 0
      new_word = "#{word}ay"
    else
      new_word = "#{word[vowel_idx..-1]+word[0..vowel_idx-1]}ay"
    end

    word[0].upcase == word[0] ? new_word.capitalize : new_word
  end

  pig_latin.join(" ")
end

def first_vowel(word)
  starting_letters = word.downcase[0..2]
  if starting_letters == "sch"
    return 3
  elsif starting_letters.include?("qu") && isvowel?(word[0]) == false
      return word.index("qu")+2
  else
    word.each_char.with_index do |char,idx|
      return idx if isvowel?(char)
    end
  end
end

def isvowel?(char)
  vowels = "aeiou"
  vowels.include?(char.downcase)
end
